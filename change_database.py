import sqlite3
con = sqlite3.connect("jukebox.sqlite3")
c = con.cursor()
# A simple example of how to change the database
# Here, we add an "action" to the log table
# Bear in mind, you should also modify jukebox/src/sql_-schemas
string = """
ALTER TABLE log 
ADD `action` text
check ( action in ('ADD', 'REMOVE') )
NOT NULL DEFAULT 'ADD'"""
c.execute(string)
con.commit()
con.close()
