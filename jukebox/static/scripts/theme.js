/**
 * This right here technically works, but looks very bad
 * (especially since it keeps moving the layout)
 */
// $(".card").each((i, obj) => {
//     obj.addEventListener("mouseover", () => {
//         console.log(`Entering ${obj.id}`)
//         $("#newStyle").attr("href", `${obj.id}`)
//         $("#newStyle").prop('disabled', false);
//         $("#curStyle").prop('disabled', true);
//     })
//     obj.addEventListener("mouseleave", () => {
//         console.log(`Leaving ${obj.id}`)
//         $("#newStyle").prop('disabled', true);
//         $("#curStyle").prop('disabled', false);
//     })
// })

function postNewTheme(themeName) {
    // First we need to send the request
    const form = document.createElement('form')
    form.setAttribute('method', "post")
    form.setAttribute('action', "theme")
    const input = document.createElement('input')
    form.style.display = 'hidden'
    input.type = "text"
    input.name = "theme"
    input.value = themeName
    input.style.display = 'hidden'
    form.appendChild(input)
    document.body.appendChild(form)
    form.submit()
    input.remove()
    form.remove()

    // Then we need to change locally, so that we don't need to change the theme.
    console.log(`Changing style for ${themeName}`)
    $("#newStyle").attr("href", `/static/themes/custom/${themeName}`)
    $("#newStyle").prop('disabled', false);
    $("#curStyle").prop('disabled', true);
}

$(function () {
    if (!isMobile) {
        $('.card a').miniPreview({prefetch: 'pageload'});
    }
    $(".card a").attr('href', 'javascript:void(0)');
});
