import math

from jukebox.src.Track import Track
from jukebox.src.User import User


def make_header(header_list: list[str]) -> str:
    """
    Generate a header for a table.
    :param header_list: List of the names of the columns of the table.
    :return: HTML Code for the <thead>
    """
    header = ""
    for h in header_list:
        header += f"<th>{h}</th>"
    return f"<thead><tr>{header}</tr></thead>"


def create_html_users(database, date=0, nbr: int = 10, track: str = None) -> str:
    """
    Creates an HTML table displaying for each user its track count.
    :param database: Database
    :param date: Max date for the logs to be considered.
    :param nbr: Max size of the generated table.
    :param track: Filter by a certain track name.
    :return: HTML Code for the table.
    """
    counts_per_user = User.getUserCounts(database, nbr, date=date, track=track)
    body = ""
    for (user, count) in counts_per_user:
        body += f"""
        <tr>
            <td><a href="/statistics/user/{user}">{user}</a></td>
            <td>{count}</td>
        """
        if count > 100:
            body += f"<td>{math.floor(User.getDiversityScore(database, user) * 100)}%</td>"
        body += "</tr>"
    return f"""<table>{make_header(["User", "Count", "Score"])}<tbody>{body}</tbody></table>"""


def create_html_tracks(database, date=0, nbr=10, user: str = None):
    """
    Creates an HTML table displaying for each track its play count.
    :param database: Database
    :param date: Max date for the logs to be considered.
    :param nbr: Max size of the generated table.
    :param user: Filter by a certain user's id.
    :return: HTML Code for the table.
    """
    counts_per_track = Track.get_list_track_counts(database, nbr, date=date, user=user)
    body = ""
    for (track, count, track_id) in counts_per_track:
        body += f"""
        <tr>
            <td><a href="/statistics/track/{track_id}">{track}</a></td>
            <td>{count}</td>
        </tr>
        """
    return f"""<table>{make_header(["Track", "Count"])}<tbody>{body}</tbody></table>"""


def create_history_tracks(database, nbr: int = 50):
    """
    Creates an HTML table displaying the history of the tracks played.
    :param database: Database.
    :param nbr: Max size of the generated table.
    :return: HTML Code for the table.
    """
    track_history = Track.get_history(database, nbr)
    body = ""
    for (track, track_id, user) in track_history:
        body += f"""
        <tr>
            <td><a href="/statistics/track/{track_id}">{track}</a></td>
            <td>{user}</td>
        </tr>
        """
    return f"""<table>{make_header(["Track", "Added by"])}<tbody>{body}</tbody></table>"""
