CREATE TABLE "users" (
    "id" INTEGER PRIMARY KEY,
    "user" TEXT NOT NULL,
    "pass" TEXT NOT NULL,
    "theme" TEXT,
    "count" INT NOT NULL DEFAULT 0
);
