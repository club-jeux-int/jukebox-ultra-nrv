#!/bin/python3
from passlib.handlers.pbkdf2 import pbkdf2_sha256

from jukebox import app
import os

if __name__ == "__main__":
    # cleanup leftovers
    if os.path.exists("mpv.socket"):
        os.remove("mpv.socket")
    temp_download_path = app.config["TEMP_DOWNLOAD_PATH"]
    if os.path.isfile(temp_download_path):
        os.remove(temp_download_path)
    if not os.path.exists(temp_download_path):
        os.makedirs(temp_download_path)
    for file in os.listdir(temp_download_path):
        os.remove(temp_download_path + file)
    # create database if it doesn't exists and back it up else
    if not os.path.exists(app.config["DATABASE_PATH"]):
        app.logger.info("Database nonexistent, creating schema")
        with app.database_lock:
            import sqlite3

            conn = sqlite3.connect(app.config["DATABASE_PATH"])
            c = conn.cursor()

            path_sql = "jukebox/src/sql-schemas/"
            with open(path_sql + "schema-users.sql", 'r') as f:
                schema_users = f.read()
            c.execute(schema_users)
            with open(path_sql + "schema-log.sql", 'r') as f:
                schema_log = f.read()
            c.execute(schema_log)
            with open(path_sql + "schema-track-info.sql", 'r') as f:
                schema_track_info = f.read()
            c.execute(schema_track_info)
            c.execute("""INSERT INTO users ("user", "pass") VALUES (?,?)""", ("local", pbkdf2_sha256.hash("local")))
            conn.commit()
            conn.close()
    else:
        with app.database_lock:
            os.system(f'rm {app.config["DATABASE_PATH"]}.backup')
            os.system(f'cp {app.config["DATABASE_PATH"]} {app.config["DATABASE_PATH"]}.backup')
    # run the flask app
    app.secret_key = "ThisMightBeThePlaceToSetTheSecretKey"
    # It's actually not used at all, so who cares
    app.run(host=app.config["LISTEN_ADDR"], port=app.config["LISTEN_PORT"])
