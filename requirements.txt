Flask >= 3.0
Flask-WTF >= 1.2
flask-profiler
sqlalchemy
python-mpv >= 1.0
youtube-dl >= 2021.04.17
yt-dlp
passlib
pyalsaaudio >= 0.9
isodate
tinytag
cachetools
requests >= 2.27
