#!/bin/python3
import sqlite3
import sys

if len(sys.argv) != 3:
    print("This scripts uses exactly two arguments:")
    print("./merge_users.py <receivingAccount> <deletedAccount>")
    exit(1)
receivingAccount = sys.argv[1]
deletedAccount = sys.argv[2]

con = sqlite3.connect("jukebox.sqlite3")
c = con.cursor()
c.execute("SELECT id FROM users WHERE user = ?", (receivingAccount,))
receivingAccountID = c.fetchone()[0]
c.execute("SELECT id FROM users WHERE user = ?", (deletedAccount,))
deletedAccountID = c.fetchone()[0]

print(f'Merging "{deletedAccount}" ({deletedAccountID}) into "{receivingAccount}" ({receivingAccountID})')
c.execute("UPDATE log SET userid=? WHERE userid = ?", (receivingAccountID, deletedAccountID))
c.execute("""
UPDATE users 
SET count = (SELECT COUNT(log.id) FROM log WHERE users.id = log.userid AND log.action = 'ADD')
WHERE id = ?
""", (receivingAccountID,))
c.execute("DELETE FROM users WHERE id = ?", (deletedAccountID,))
con.commit()
con.close()
